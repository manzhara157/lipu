# LIPU (Linux Інструкція Після Установки) 

Для особистого і не тільки користування.
Це пам'ятка по налаштуванню системи після установки. Оновлення драйверів, ядра, особливості встановлення деяких програм,
 список програм та розширень, якими Я користуюсь. Я маю досвід користування системі на основі Debian, Ubuntu і Manjaro 
 на різних DE (Gnome, KDE, Cinnamon, Pantheon). Якщо спосіб не універсальний, то Я уточнюватиму для якого дистрибутиву
  або DE це спрацює. 

## Зміcт
- [1. Оновлення системи](https://gitlab.com/manzhara157/lipu#1-%D0%BE%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%BD%D1%8F-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B8)
- [2. Оновлення відео драйверів Intel\AMD](https://gitlab.com/manzhara157/lipu#2-%D0%BE%D0%BD%D0%BE%D0%B2%D0%BB%D0%B5%D0%BD%D0%BD%D1%8F-%D0%B2%D1%96%D0%B4%D0%B5%D0%BE-%D0%B4%D1%80%D0%B0%D0%B9%D0%B2%D0%B5%D1%80%D1%96%D0%B2-intelamd)
- [3. Налаштування Gnome Shell.](https://gitlab.com/manzhara157/lipu#3-%D0%BD%D0%B0%D0%BB%D0%B0%D1%88%D1%82%D1%83%D0%B2%D0%B0%D0%BD%D0%BD%D1%8F-gnome-shell)
- [4. WPS Office](https://gitlab.com/manzhara157/lipu#4-wps-office)
- [5. CoreCtrl](https://gitlab.com/manzhara157/lipu#5-corectrl)
- [6. Steam](https://gitlab.com/manzhara157/lipu#6-steam)
- [7. Conky](https://gitlab.com/manzhara157/lipu#7-conky)
- [8. Telegram](https://gitlab.com/manzhara157/lipu#8-telegram)
- [9. Список корисних програм](https://gitlab.com/manzhara157/lipu#9-%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BA%D0%BE%D1%80%D0%B8%D1%81%D0%BD%D0%B8%D1%85-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC)
- [10. Налаштування Cinnamon ](https://gitlab.com/manzhara157/lipu#9-%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BA%D0%BE%D1%80%D0%B8%D1%81%D0%BD%D0%B8%D1%85-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC)
- [11.HUD в іграх ](https://gitlab.com/manzhara157/lipu#11-hud-в-іграх)
- [12. Зависання системи на процесорах Intel](https://gitlab.com/manzhara157/lipu#12-%D0%BF%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B8-%D0%B7-%D0%B2%D0%B1%D1%83%D0%B4%D0%BE%D0%B2%D0%B0%D0%BD%D0%BE%D1%8E-%D0%B3%D1%80%D0%B0%D1%84%D1%96%D0%BA%D0%BE%D1%8E-%D0%B2%D1%96%D0%B4-intel-baytrail)
- [13. Кастомна розкладка клавіатури](https://gitlab.com/manzhara157/lipu#12-%D0%BF%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B8-%D0%B7-%D0%B2%D0%B1%D1%83%D0%B4%D0%BE%D0%B2%D0%B0%D0%BD%D0%BE%D1%8E-%D0%B3%D1%80%D0%B0%D1%84%D1%96%D0%BA%D0%BE%D1%8E-%D0%B2%D1%96%D0%B4-intel-baytrail)

## 1. Оновлення системи

Або через "Оновлення програм" або через термінал
 
**Debian/Ubuntu**
```
sudo apt update
sudo apt upgrade -y
```

## 2. Оновлення відео драйверів Intel\AMD

#### **2.1 Ubuntu**

Репозиторій kisak-mesa PPA (https://launchpad.net/~kisak/+archive/ubuntu/kisak-mesa) Mesa для відеокарт AMD/Intel,
 з підтримкою AMD Valve ACO

(Взято з BZU. Автор: Яцина М. А.)
```
sudo add-apt-repository ppa:kisak/kisak-mesa
sudo apt install -f --reinstall mesa*
sudo apt install -f --reinstall libvulkan1 libvulkan1:i386 libvulkan-dev libvulkan-dev:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386 vulkan-utils
```

Увага: початково ACO від valve відключений! Щоб його задіяти для API Vulkan потрібно:
>Если запускаем из консоли игру и т.д. : RADV_PERFTEST=aco /path/to/my_vulkan_app , для примера: RADV_PERFTEST=aco vkcube

>Если через Steam: в одной из игр steam, зайдите в свойства игры в steam, потом нажмите на установить параметры запуска и добавьте в строку это: RADV_PERFTEST=aco %command%

Також є інший репозитарій від oibaf. (https://launchpad.net/~oibaf/+archive/ubuntu/graphics-drivers)
```
sudo add-apt-repository ppa:oibaf/graphics-drivers
sudo apt update
sudo apt upgrade
```

Починаючи з версії 20.2 Mesa містить підтримку ACO. Але все ж в цих репозитаріях міститься найсвіжіші версії. 


#### **2.2 Debian**

Підключаємо Sid репозиторії, оскільки там найновіший Mesa драйвер. На 2020 рік це було важливо.

1.  Відкриваємо файл:
```
sudo nano /etc/apt/source.list
```
1.  Додаємо в кінець файлу рядок:
  ```
  deb http://ftp.de.debian.org/debian sid main
```
Зберігаємо (Ctrl+O) і закриваємо (Ctrl+X) файл.

1.  Оновлюємо сховище:
 ```
sudo apt update
```
  1.  Перевстановлюємо драйвер
  ```
sudo apt install -f --reinstall mesa*
sudo apt install libgl1:i386 mesa-vulkan-drivers:i386 mesa-vulkan-drivers
```
>Примітка №1: Можна за одно оновити ядро до актуального. Для цього достатньо встановити пакунки linux-header-версія_ядра і linux-image-версія_ядра. Для того, щоб дізнатись, які версія ядра доступні для встановлення виконайте в терміналі команду:
>```
>apt search linux-header
>```
  
>Примітка №2: після оновлення драйвера не забудьте знову зайти в файл source.list (перший крок) і закоментувати репозиторії Sid (а можна і видалити їх) з подальшим оновленням сховищ (третій крок).

>Примітка №3: для перевірки версії mesa драйвера скористайтесь командою:
>```
>glxinfo | grep OpenGL
>```
 
 #### 2.3. Встановлення игрового ядра LIQUORIX
 
 Цей крок для игрового комп'ютера. Ядро оптимізоване під виведення графіки. Спочатку треба приєднати репозитарії:
 
Debian
 ```
 codename="$(find /etc/apt -type f -name '*.list' | xargs grep -E '^deb' | awk '{print $3}' | grep -Eo '^[a-z]+' | sort | uniq -c | sort -n | tail -n1 | grep -Eo '[a-z]+$')" && sudo apt-get update && sudo apt-get install apt-transport-https && echo -e "deb http://liquorix.net/debian $codename main\ndeb-src http://liquorix.net/debian $codename main\n\n# Mirrors:\n#\n# Unit193 - France\n# deb http://mirror.unit193.net/liquorix $codename main\n# deb-src http://mirror.unit193.net/liquorix $codename main" | sudo tee /etc/apt/sources.list.d/liquorix.list && curl https://liquorix.net/linux-liquorix.pub | sudo apt-key add - && sudo apt-get update
```
Ubuntu
```
sudo add-apt-repository ppa:damentz/liquorix && sudo apt-get update
```
Ядро Liquorix можна встановити за допомогою мета-пакетів. Це гарантуватиме, що останнє ядро буде встановлено під час кожного оновлення.
 
 64-bit:

 ```
 sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64
 ```
  Посилання на сайт [LIQUORIX KERNEL](https://liquorix.net)
  
## 3. Налаштування Gnome Shell.

#### **3.1 Встановлення Tweak Tool і відновлення роботоздатності розширень.**
```
sudo apt install -y gnome-tweak-tool
```
Інколи розширення не  працює одразу. На сайті будуть підказки, але Я продцблюю команду і тут.
```
sudo apt install -y chrome-gnome-shell
```
#### **3.2 Gnome Extensions**
Список розширень якими я користуюсь в Gnome DE:
- Dash to Dock
- Dynamic Panel Transparency
- Remove Shield
- Remove Dropdown Arrows
- Fully Transparent Top Bar
- Freon
- Status Area Horizontal Spacing

## 4. WPS Office

Можливо, вам знадобляться шрифти, типу, Times New Roman. Для того щоб їх встановити вводимо в терміналі:
```
sudo apt-get install ttf-mscorefonts-installer
```
Далі завантажуємо останню версію WPS Office з [офіційного сайту](https://linux.wps.com/#). Для Ubuntu/Debian підходять .deb пакети.
.deb пакети встановлюються за допомогою dpkg (є і програми з GUI, але Я користуюсь термінальною програмою). Скачати теж можна через
термінал.

```
cd
wget http://wdl1.pcfg.cache.wpscdn.com/wpsdl/wpsoffice/download/linux/9505/wps-office_11.1.0.9505.XA_amd64.deb
sudo dpkg -i wps-office_11.1.0.9505.XA_amd64.deb
rm wps-office_11.1.0.9505.XA_amd64.deb
```
В wps office є проблемка з гарячими клавішами. Вони працюють тільки на англійській розкладці. Це дико незручно і це 
можна виправити.
Для цього відкриваємо файл:
```
sudo nano /opt/kingsoft/wps-office/office6/res/wpscommon.kuip 
```
Шукаємо рядок
```
<KxOleLegacyTriggerCommand hotKey="с" ksoCmd="Copy" aid="19" idMso="Copy" icon="Copy" controlType="0" shortcutHint="Ctrl+С" id="Copy" customTip="Copy" spaceHint="autocompact" text="@Copy" iconQat="CopyQat" rbHotKey="C"/>
```
і копіюємо і вставляємо його нижче. Тепер просто замінюємо англійську букву "с" на букву "с" з кирилиці. Так само можна
 зробити для команди Paste. Я змінив букви в hotKey, shortcutHint і rbHotKey.


>Для пошуку в nano натискаємо Ctrl+W

Детальна інструкція з встановлення аналогу MS Office в статті за 
посиланням [посиланням](https://www.nibbl.ru/linux/linuxday3-besplatnyj-analog-office-na-linux-i-windows.html).
Скажімо "Дякую!" за статтю nibbl.

## 5. CoreCtrl 
Програма CORECTRL для автоматичного керування вентиляторами і overclocking AMD

### 5.1 Інсталяція
#### Не актуальний спосіб
Я так і не зміг зібрати з коду на Ubuntu 18.04 (і деревативах на його базі, типу Mint) i Debian 10, 
тому роблю неправильно. Починаючи з версії 19.04 CoreCtrl можна поставити з репозитарія Mesa Almost Stable. 
Але для вставлення треба деякі пакети з новіших версій. Тому треба під'єднати репозиторії 19.04+.
Відкриваємо файл з репозитаріями:
```bash
sudo nano /etc/apt/source.list
```
Дописуємо в кінець рядок:
```$xslt
deb http://cz.archive.ubuntu.com/ubuntu disco main universe
deb http://ppa.launchpad.net/ernstp/mesarc/ubuntu eoan main 
deb-src http://ppa.launchpad.net/ernstp/mesarc/ubuntu eoan main
```
Зберігаємо (Ctrl+O) і закриваємо (Ctrl+X) файл.
Оновлюємо сховище:
 ```
sudo apt update
```
Після, встановлюємо CoreCtrl:
```$xslt
sudo apt install corectrl
```
Після встановлення зайдіть в файл з репозитаріями та зітріть або закоментуйте рядки, які були додані. 
Збережіть файл та обновіть сховище.
Якщо ви ще користуєтесь 18.04 то такий спосіб використовуйте на свій страх і ризик. Перед його використанням 
рекомендую спробувати спосіб нижче. На нових версіях Ubuntu таких проблем не повинно бути. 
```
Залишу як запасний варіант
CoreCtrl более продвинутая утилита чем radeon-profile и больше подходит для видеокарт VEGA 56\64: https://gitlab.com/corectrl/corectrl
ставить будем из исходников, перед установкой на 19.04 нужно установить дополнительные пакеты:
sudo apt install extra-cmake-modules libqt5charts5 libqt5charts5-dev qttools5-dev libkf5auth-dev libkf5archive-dev libbotan-2-dev qtdeclarative5-dev qml-module-qtquick-controls hwdata qml-module-qt-labs-platform qml-module-qtcharts

qtdeclarative5-dev qtdeclarative5-dev-tools qttools5-dev qttools5-dev-tools tzdata vulkan-tools vulkan-utils

qml-module-qtqml-models2 qml-module-qtquick-controls2
##############если у вас ubuntu 18.04\ linux mint 19.x#############################
#нужно сначала настроить компилятор, поэтому ставим 8 версии из репы ниже
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install --reinstall gcc-8 g++-8
sudo apt install --reinstall gcc-7 g++-7
#после обновляем приоритеты, что бы 8 версия была основной
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 700 --slave /usr/bin/g++ g++ /usr/bin/g++-7
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 800 --slave /usr/bin/g++ g++ /usr/bin/g++-8
#после нужно проверить что теперь в приоритете 8 версия компилятора
sudo update-alternatives --config gcc
#если это не так выбирайте этот пункт
* 0 /usr/bin/gcc-8 800 auto mode
#все можно продолжить сборку
###################################################################################
далее сборка
cd
git clone https://gitlab.com/corectrl/corectrl.git
cd corectrl
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
make
sudo make install
В процессе первичной проверки перед сборкой cmake может ругнуться, что еще не хватает пакетов, гуглите что ему нужно
для разгона VEGA 56\64 обязательно нужно выполнить пункт: 5.0b AMD GPU OVERCLOCKING / UNDERVOLTING (rx400\500+vega 56\64)
По ссылке выше, на официальном вики есть видео с описанием функций.
Автор: Яцына М.А.﻿
```
#### Актуальний спосіб
В Ubuntu 20.04 такої проблеми вже немає. Встановити CoreCTRL можна з репозитарія.

```
sudo add-apt-repository ppa:ernstp/mesarc
sudo apt install corectrl
```

Однак, раджу після встановлення програми відключати репозитарій, тому що в ньому є свій mesa драйвер. 
Драйвер краще брати з іншого місця.  

### 5.2 Автозапуск без пароля
Залишилось зробити, щоб CoreCtrl стартував разом із системою. В терміналі прописуємо:
```
cp /usr/share/applications/org.corectrl.corectrl.desktop ~/.config/autostart/org.corectrl.corectrl.desktop
```
Тепер він буде запускатись при в ході до облікового запису, але при зупуску потрібно буде вводити пароль. 
Є спосіб, як це виправити. Спочатку треба дізнатись версію polkit.
```
pkaction --version
```
#### Якщо версія **polkit < 0.106**
Створюємо файл такою командою:
```
sudo nano /etc/polkit-1/localauthority/50-local.d/90-corectrl.pkla
```
Записуємо в файл наступне:
```
[User permissions]
Identity=unix-group:your-user-group
Action=org.corectrl.*
ResultActive=yes
```
#### Якщо версія **polkit >= 0.106** 
Створюємо файл такою командою:
```
sudo nano /etc/polkit-1/rules.d/90-corectrl.rules
```
Записуємо в файл наступне:
```bash
polkit.addRule(function(action, subject) {
    if ((action.id == "org.corectrl.helper.init" ||
         action.id == "org.corectrl.helperkiller.init") &&
        subject.local == true &&
        subject.active == true &&
        subject.isInGroup("your-user-group")) {
            return polkit.Result.YES;
    }
});
```

> Примітка!!! Щоб це спрацювало замініть **your-user-group** на назву вашою користувацькою групи!

На цьому кроці Я завжди зупиняюсь, але якщо ви хочете ще і розігнати свою відеокарту, то про це можна почитати на 
офіційному [git репозитарії](https://gitlab.com/corectrl/corectrl/-/wikis/home) в 
пункті [Setup](https://gitlab.com/corectrl/corectrl/-/wikis/Setup).

## 6. Steam
### **Ubuntu**

На Ubuntu достатньо прописати в терміналі команду:
```$xslt
sudo apt install steam
```
### **Debian**

В Debian є нюанси.
Спочатку треба під'єднати закриті пакунки. Заходимо в файл сховищ (в Debian тільки так і підключаються нові репозитарії).
```$xslt
sudo nano /etc/apt/source.list
```
Нам треба ось цей рядок
```
deb http://deb.debian.org/debian/ buster main 
```
В нього ми дописуємо contrib і non-free. Отримуємо щось отаке:
```
deb http://deb.debian.org/debian/ buster main contrib non-free
```
Зберігаємо (Ctrl+O) і закриваємо (Ctrl+X) файл.
Підключаємо 32-бітну архітектуру та оновлюємо сховище:
```$xslt
sudo dpkg --add-architecture i386
sudo apt update
```
Встановлюємо додаткові бібліотеки і драйвера для AMD:
```$xslt
sudo apt install libgl1:i386 mesa-vulkan-drivers:i386 mesa-vulkan-drivers
```
От тепер можна встановити Steam:
```$xslt
sudo apt install steam
```
>Привітка №1. Після встановлення Я, зазвичай, видаляю non-free і contrib, але це не опов'язково.

>Примітка №2. Якщо з'являться додаткоі питання, то ось посилання на офіційну [статтю](https://wiki.debian.org/Steam#Graphics), де була взята ця інформація.

## 7. Conky
Conky або в народі "коньки" це простий і водночас потужний hud для моніторингу стану системи. 
Для встановлення в терміналі прописуємо:
```$xslt
sudo apt install conky -y
```
В інтернеті можна знайти сотні різних оформлень. В мене можна знайти налаштування мого Conky для 
Gnome(підходить для Cinnamon,Pantheon) і KDE. Завантажуєте потрібний файл в ~/ (хто не знає так позначається домашній каталог.
повний шлях /home/username/). Після скачування відкриваєте файл і змінюєте назви сенсорів на ваші. О! І не забудьте додати 
Conky в автозапуск.

**Conky Cinnamon**
![Cinnamon](Cinnamon.png "Mint")

**Conky Pantheon**
![Pantheon](Pantheon.jpg "Elementary OS")

**Conky Gnome**
![Ubuntu](Ubuntu.png "Ubuntu")

**Conky KDE**
![Ubuntu](kde.png "Ubuntu")

## 8. Telegram

Telegram популярний месенджер, яким користуються більшість моїх знайомих. На Ubuntu telegram можна поставити з магазину 
програм (типу як в телефонах). Але там він ставиться як снап або флетпак пакет. Особисто мені це не подобається, тому Я 
ставлю завжи з архіву, який можна завантажити на офіційному сайті. Для встановлення версії 1.9.21 введіть такі команди:
```
cd /opt/
sudo wget https://updates.tdesktop.com/tlinux/tsetup.1.9.21.tar.xz
sudo tar -xvf tsetup.1.9.21.tar.xz
sudo rm tsetup.1.9.21.tar.xz
/opt/Telegram/Telegram
```
Якщо ви хочете мати однотонні іконки в треї, пройдіть за посиланням. 
https://github.com/aldomann/telegram-systray-icons

https://github.com/bilelmoussaoui/Hardcode-Tray

## 9. Список корисних програм
Список програм, якими Я користуюсь. Їх Я ставлю завжди.
 - mc (midnight commander. Менеджер файлі)
 - htop (Менеджер процесів)
 - git (Контроль версій. Знадобиться, коли будите щось ставити з source code. CoreCtrl, наприклад)
 - wget (Інколи не встановлений з коробки. Програма для завантаження файлів за посиланням.)
 - gparted (Дял gtk, тобто Gnome, Pantheon, Cinnamon, Mate. Менеджер дисків)
 - tilix (Емулятор терміналу. Трохи кращий ніж стандартний)
 - Disk Usage Analyzer (Він же baobab. Аналіз простору диска)
 
 #### Ubuntu/Debian
 > sudo apt install mc htop git wget gparted tilix baobab
#### Manjaro
>sudo pacman -S mc htop git wget gparted tilix
 
## 10. Налаштування Cinnamon 
Інструкція, як змінити значок меню:

http://compizomania.blogspot.com/2018/12/linux-mint-191-cinnamon_15.html

## 11. Hud в іграх
### 11.1 MangoHud
Встановлення MangoHUD з джерельного коду:

Завантажеємо код з github:
```
git clone --recurse-submodules https://github.com/flightlessmango/MangoHud.git
cd MangoHud
```
Після завантаження виконайте наступні команди:
```
./build.sh build
./build.sh package
``` 
> Примітка! Якщо буде повідомлення про невстановлені залежності, встановіть їх.

Встановіть MangoHUD виконавши наступну команду:
```
./build.sh install
```

Для детального налаштування можна модифікувати конфігураційний файл: 
```
nano ~/.config/MangoHud/MangoHud.conf
```
Детально про налаштування можна почитати на офіційній сторінці [GitHub](https://github.com/flightlessmango/MangoHud)

### 11.2 GalliumHUD
GalliumHUD працює лише з OpenGL. 
Мій Hud:
GALLIUM_HUD=.h80.w110cpu0+cpu1+cpu2+GPU-load+fps:100,.h80.w110.d.c70sensors_temp_cu-amdgpu-pci-0100.edge=GPU-Temp+sensors_temp_cu-nct6776-isa-0290.CPUTIN=CPU-Temp
Для застосування в Steam потрібно цей рядок додати в параметри запуску. Приклад:
GALLIUM_HUD=simple,fps,cpu,GPU-load %command%


### 11.3 Як увімкнути HUD?
В Steam достатньо в параметрах запуску дописати 
"mangohud %command%" або "GALLIUM_HUD=...набір_параметрів... %command%" без лапок

В Lutris в налаштуваннях, на влкаладці системних опцій, додати Eyvironment variable, і в полі Key 
написати mangohud або GALLIUM_HUD, в полі Value написати 1 або набір параметрів.

## 12. Проблеми з вбудованою графікою від Intel (BayTrail)

В мобільних процесорах від Intel є проблема, яка виявляється як зависання системи. Якщо ви щасливий
 володар такого [процесора](https://ark.intel.com/content/www/ru/ru/ark/products/codename/55844/bay-trail.html),
 ось інструкція по боротьбі з зависаннями.
 
 В терміналі відкриваєте на редагування файл grub:
 ```
sudo nano /etc/default/grub
```  
В рядок GRUB_CMDLINE_LINUX_DEFAULT= дописуєте intel_idle.max_cstate=1. Має вийти щось на зразок цього:

>GRUB_CMDLINE_LINUX_DEFAULT="quiet splash intel_idle.max_cstate=1"

quiet і splash стандартні параметри. Якщо їх нема, то дописувати не треба. зберігаємо комбінацією клавіш 
Ctrl+o і виходимо комбінацією Ctrl+x. 

Оновлюємо конфігурації Grub:
```
sudo update-grub
```

Після перезавантаження проблема з зависаннями моє зникнути.

Photoshop for linux
https://github.com/Gictorbit/photoshopCClinux

## 13. Кастомна розкладка клавіатури

### Для чого?
Тут 2 причини. Перша полягає у тому, що Я часто перемикаюсь між українською та акглійською розкладкою. Це спричиняю плутанину з введенням розділових знаків. Тому в першу чергу Я переслідував ціль уніфікувати введення крапки та коми, як часто вживаних розділоваих знаків. Дурка причина пов'язана з освоєнням Vim. навігація в Vim здійнюється за допомогою клавіш hjkl. І це зручніше ніж здається. В кастомній розкладці Я реалізував таку ж навігацію через третій рівен введення.

### Як зробити?
![layout](KB_Ukrainian_Unicode.png)

Чорним показано звичайну розкладку, Червоним показано третій рівень. Я включаю третій рівень кнопкой Caps Lock.
Текст нижче потрібно дописати в файл /usr/share/X11/xkb/symbols/ua
```
partial alphanumeric_keys
xkb_symbols "uam" {

    include "ua(winkeys)"
    name[Group1]= "Ukrainian (Manzhara Custom)";

    key <TLDE> { [      apostrophe,           U02BC,          U0301,          asciitilde  ] };  // Apostrophe and Stress symbol
    key <AE01> { [               1,          exclam,    onesuperior                       ] };
    key <AE02> { [               2,        quotedbl,    twosuperior,               U2019  ] };  // single quote used often as an apostrophe (deprecated)
    key <AE03> { [               3,      numerosign,          U00A7,               U20B4  ] };  // Paragraph and Hryvnia sign
    key <AE04> { [               4,       semicolon,         dollar,            EuroSign  ] };
    key <AE05> { [               5,         percent,         degree                       ] };
    key <AE06> { [               6,           colon,           less                       ] };
    key <AE07> { [               7,        question,        greater                       ] };
    key <AE08> { [               8,        asterisk, enfilledcircbullet                   ] };
    key <AE09> { [               9,       parenleft,    bracketleft,           braceleft  ] };
    key <AE10> { [               0,      parenright,   bracketright,          braceright  ] };
    key <AE11> { [           minus,      underscore,         emdash,              endash  ] };
    key <AE12> { [           equal,            plus,       notequal,           plusminus  ] };

    key <AD01> { [ Cyrillic_shorti, Cyrillic_SHORTI, Ukrainian_yi,    Ukrainian_YI  ] };
    key <AD02> { [    Cyrillic_tse,    Cyrillic_TSE, End  ] };
    key <AD03> { [      Cyrillic_u,      Cyrillic_U, Byelorussian_shortu, Byelorussian_SHORTU  ] };
    key <AD04> { [     Cyrillic_ka,     Cyrillic_KA,     registered                       ] };  // Registered tm
    key <AD05> { [     Cyrillic_ie,     Cyrillic_IE,    Cyrillic_io,         Cyrillic_IO  ] };

//  key <AD12> { [    Ukrainian_yi,    Ukrainian_YI, Cyrillic_hardsign,Cyrillic_HARDSIGN  ] };
    key <AD12> { [    Cyrillic_be,     Cyrillic_BE, Cyrillic_hardsign,Cyrillic_HARDSIGN  ] };

//  key <AC02> { [     Ukrainian_i,     Ukrainian_I,  Cyrillic_yeru,       Cyrillic_YERU  ] };
    key <AC02> { [     Ukrainian_i,     Ukrainian_I,  Ukrainian_yi,    Ukrainian_YI       ] };

    key <AC03> { [     Cyrillic_ve,     Cyrillic_VE, Ukrainian_ie,    Ukrainian_IE  ] };
    key <AC06> { [     Cyrillic_er,     Cyrillic_ER, Left  ] };
    key <AC07> { [      Cyrillic_o,     Cyrillic_O,  Down   ] };
    key <AC08> { [     Cyrillic_el,     Cyrillic_EL, Up  ] };
    key <AC09> { [     Cyrillic_de,     Cyrillic_DE, Right  ] };


//  key <AC11> { [    Ukrainian_ie,    Ukrainian_IE,     Cyrillic_e,          Cyrillic_E  ] };
    key <AC11> { [    Cyrillic_yu,     Cyrillic_YU,     Cyrillic_e,          Cyrillic_E   ] };

    
    key <BKSL> { [ Ukrainian_ghe_with_upturn, Ukrainian_GHE_WITH_UPTURN, backslash,  bar  ] };

    key <AB02> { [    Cyrillic_che,    Cyrillic_CHE,   Delete  ] };
    key <AB03> { [     Cyrillic_es,     Cyrillic_ES,      copyright                       ] };
    key <AB05> { [     Cyrillic_i,      Cyrillic_I,    Cyrillic_yeru,       Cyrillic_YERU ] };
    key <AB06> { [     Cyrillic_te,     Cyrillic_TE,      trademark                       ] };
    //key <LatB> { [     Cyrillic_i,      Cyrillic_I,       Cyrillic_be,     Cyrillic_BE    ] };
    //key <LatE> { [     Cyrillic_u,      Cyrillic_U,		  Cyrillic_yu,     Cyrillic_YU    ] };
    //key <AB08> { [     Cyrillic_be,     Cyrillic_BE,  guillemotleft,  doublelowquotemark  ] };
    //key <AB09> { [     Cyrillic_yu,     Cyrillic_YU, guillemotright, leftdoublequotemark  ] };
    //key <AB10> { [          period,           comma,          slash,            ellipsis  ] };
    key <AB08> { [ comma,             less               ] };
    key <AB09> { [ period,            greater            ] };
    key <AB10> { [ slash,             question           ] };

    // Serbian Vukovica
    //key <AD01> { [ Cyrillic_shorti, Cyrillic_SHORTI,    Cyrillic_je,         Cyrillic_JE  ] };
    //key <AD02> { [    Cyrillic_tse,    Cyrillic_TSE,  Cyrillic_dzhe,       Cyrillic_DZHE  ] };
    //key <AC09> { [     Cyrillic_de,     Cyrillic_DE,    Serbian_dje,         Serbian_DJE  ] };
    //key <AB02> { [    Cyrillic_che,    Cyrillic_CHE,   Serbian_tshe,        Serbian_TSHE  ] };
    //key <AC08> { [     Cyrillic_el,     Cyrillic_EL,   Cyrillic_lje,        Cyrillic_LJE  ] };
    //key <AD06> { [     Cyrillic_en,     Cyrillic_EN,   Cyrillic_nje,        Cyrillic_NJE  ] };

    include "level3(ralt_switch)"
};
```

Можна налаштувати розкладку через xkb. Це легко робиться в віконних менеджерах i3/sway, просто указавши замість ua -> uam. Але в KDE Я зіткнувся з проблемою, що налаштування xkb часть від часу злітають, а варіанту розкладки нема в списку варіантів. Для того, щоб розкладку можна було встановити стандартними методами потрібно в файл /usr/share/X11/xkb/rules/evdev.xml, в розділ з українською мовою дотади такі рядки:
```
        <variant>
          <configItem>
            <name>uam</name>
            <description>Ukrainian (Manzhara Custom)</description>
          </configItem>
        </variant>
```
Після збереження файлу потрібно очистити кеш xkb. Це можна зробити командою:
```
 sudo dpkg-reconfigure xkb-data
```
Якщо все зроблено правильно, то розкладка маоє з'явитись в списку варіантів розкладок.

В ще проблема з перемиканням мови з української через Right Alt. Для вирішення цієї проблеми треба відредагувати файл **/usr/share/X11/xkb/symbols/level3**.

В файлі треба знайти `xkb_symbols "ralt_switch"` та замінити рядок
```
symbols[Group1] = [ ISO_Level3_Shift ]
```
на
```
symbols[Group1] = [ ISO_Next_Group, ISO_Level3_Shift ]
```
Готово. Тепер перемикання мови натисканням Right Alt буде спрацьовувати і для української мови.
